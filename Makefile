# requires GNU Make - this Makefile won't work with BSD Make
.ONESHELL:
SHELL = /bin/bash


MAINTNAME  = Per Weijnitz
MAINTEMAIL = per.weijnitz@arbetsformedlingen.se

CPANMODULE = Future::Mojo
ART_VER    = 1.001
ART_PKGVER = 1
ART_AWSSIG = libfuture-mojo-perl_$(ART_VER)-$(ART_PKGVER)_all.deb


# Which version of pkgbuilder used
PKGBUILDER_COMMIT = a8e3cbb


ifneq (,$(wildcard perl-lib-pkgbuilder/pkgbuilder.settings))
include perl-lib-pkgbuilder/pkgbuilder.settings
endif


RUNCONTAINER = $(CONTSYS) run -v "$(PWD)/$(PKGDIR)":/out \
		  -v $$PWD/debian:$$PWD/debian \
		  --rm -i $(IMG) \
		  --version "$(ART_VER)" --maintainer-email "$(MAINTEMAIL)" --maintainer-name "$(MAINTNAME)" --signkey "$$SIGNKEY" --cpan-module "$(CPANMODULE)" --chown $(shell id -u):$(shell id -g)


all: perl-lib-pkgbuilder/pkgbuilder.settings
	$(MAKE) build # to get include abode


build: chkkey
	$(MAKE) build-artefact sign-pkg SIGNKEY=$$SIGNKEY


build-artefact: $(PKGDIR)/$(ART_AWSSIG)


perl-lib-pkgbuilder/pkgbuilder.settings:
	git clone https://gitlab.com/arbetsformedlingen/maintained-packages/perl-lib-pkgbuilder.git
	cd perl-lib-pkgbuilder
	git checkout $(PKGBUILDER_COMMIT)


setup-image:
	mkdir -p $(PKGDIR)
	$(MAKE) -C perl-lib-pkgbuilder -f Makefile build-image


$(PKGDIR)/$(ART_AWSSIG): perl-lib-pkgbuilder/pkgbuilder.settings chkkey setup-image
	$(RUNCONTAINER) --load-debian-dir $$PWD/debian || exit 1


initialise-debian-dir: perl-lib-pkgbuilder/pkgbuilder.settings setup-image
	$(MAKE) _initialise-debian-dir # to read includes after perl-lib-pkgbuilder/pkgbuilder.settings


_initialise-debian-dir:
	$(RUNCONTAINER) --save-debian-dir $$PWD/debian || exit 1


### Just propagate calls to tool Makefile below:


sign-pkg: perl-lib-pkgbuilder/pkgbuilder.settings $(PKGDIR)/$(ART_AWSSIG) chkkey
	make -C perl-lib-pkgbuilder/ sign-pkg


chkkey: perl-lib-pkgbuilder/pkgbuilder.settings
	make -C perl-lib-pkgbuilder/ chkkey


clean: perl-lib-pkgbuilder/pkgbuilder.settings
	make -C perl-lib-pkgbuilder/ clean-pkg CLEANDIR=$(PWD)
