-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Format: 3.0 (quilt)
Source: libfuture-mojo-perl
Binary: libfuture-mojo-perl
Architecture: all
Version: 1.001-1
Maintainer: Per Weijnitz <per.weijnitz@arbetsformedlingen.se>
Homepage: https://metacpan.org/release/Future-Mojo
Standards-Version: 4.3.0
Build-Depends: debhelper (>= 11), perl
Package-List:
 libfuture-mojo-perl deb perl optional arch=all
Checksums-Sha1:
 18ce86c9f0ee48892964e0d2f82ddd2e6320de33 18925 libfuture-mojo-perl_1.001.orig.tar.gz
 c64be3f66d81e4b099c81360d9c86befbfbcccdb 4452 libfuture-mojo-perl_1.001-1.debian.tar.xz
Checksums-Sha256:
 3c0a011e712eb4de5f9df928bb9619d9909c6c3e855a631e5fc5fe8e3434f2b1 18925 libfuture-mojo-perl_1.001.orig.tar.gz
 65c0f208a490fd6d01e2c9551c8abfbe51a5f13b55f519a4133cce8a05e4c819 4452 libfuture-mojo-perl_1.001-1.debian.tar.xz
Files:
 50d3a7249995a5d206ae17a0b827fc9a 18925 libfuture-mojo-perl_1.001.orig.tar.gz
 34144b08a334788296a068aac9a5c06f 4452 libfuture-mojo-perl_1.001-1.debian.tar.xz

-----BEGIN PGP SIGNATURE-----

iQGzBAEBCgAdFiEE6niSkXgqx29tzEorbrTKOWRu0agFAmCIKtMACgkQbrTKOWRu
0aj7VwwAmSzPlhwqZci6FHX01Re2PTifatIIR8YGz4cudImM0R4V0Wd+WdQv8VJO
7D1kByeh8yukwfUyJWNjA5qZcZcTM0Ukfb0pBx9xH1JNFHhF8C2ZDJMCJB4GHB5o
HMNNz+ao+gb/seXhYF72Ry7m+Zh0uPC8fdfijH72zKzVV7WedfP5gFSedQR0TPxI
HBMy+5QiLFf59jMnrlRY+Nuw10COm8mwLD+Fr99JtK4V03tUZSpCk4yu3YV2NcpQ
hTqbw1NE+xHvux6wlj77wk2FtJclhecpBiMmBcZ4UJglEUzMQuS25qXv16GupESm
V4HahMYw6jyc4dqX4hiW46n9mfrJxIdyKzjdiJ+sa0YmpM41xXg49SGZACpX6A7y
rqOZcyfyipenO1uk7+0qjpG1/0SRekIL2zAy2qKMMqialO0rS6hMTbfLv+VBd17A
EE5kXn+OixoDt48wgWhk80vwM/NTxaJZ/rrrkiIwqhP/41H/OqMAFH01toXVH0jw
IpcWo4iz
=lb4C
-----END PGP SIGNATURE-----
